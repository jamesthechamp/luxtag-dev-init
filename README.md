
## Goal
Application to retrieve transactions for given address from NIS distributed system.  

#####Note

Here transaction are extracted and then a websocket connection is made to get new transaction in realtime.

##### Linting
 project uses eslint to lint Javascript  

#### Stack  
Javascript, NodeJS, VueJS 
 
## Build Setup
Requires a recent installation of NodeJS and npm.

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080. (ignore the compiler warnings)
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

Demo:

To retrieve transaction with address:

![Alt Text](static/demo.gif)

